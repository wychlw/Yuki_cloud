const express = require('express');
const fs = require('fs').promises;
const fs_sync = require('fs');
const busboy = require('busboy');

const config = require('./config');
const file_system = require('./components/file_system');

var app = express();

//init

/**
 * don't know why sqlite3 can not use
 *so share func is not avaliable now
 *end init
  */

/**
 * api:
 * 
 * /#/* is for build single_page application
 * /f/* is for get file data
 * /api/* is discribed below
 * /d/* is for download file (if it is a folde, then goto /f/*)
 * /r/* id for remove file
 * /s/* is for share file 
 * /u/* is for upload file
 *
 */

/**
 * /api/*:
 * some function may not so important or just very small, so is unfit to make a new top list specfic for it.
 * so, they will be put to here
 * 
 * /api/login login
 * /api/create_folder create_folder
 */

app.get('/f/*', (req, res) => {
    console.log("query file with path:" + req.path);
    console.log("and " + JSON.stringify(req.query));
    var path = './contents' + req.path.substr(2);
    var password = req.query.password ? req.query.password : null;
    var user = req.query.user ? req.query.user : null;
    file_system.read_file(path, { 'password': password, 'user': user }).then(
        data => {
            console.log(data);
            res.status(200).json(data);
        }, reason => {
            console.log(reason);
            res.status(reason[0]).json(reason);
        }
    );
});

app.get('/d/*', (req, res) => {
    console.log("download file with path:" + req.path);
    console.log("and " + JSON.stringify(req.query));
    var path = './contents' + req.path.substr(2);
    var password = req.query.password ? req.query.password : null;
    var user = req.query.user ? req.query.user : null;
    file_system.read_file(path, { 'password': password, 'user': user }).then(
        data => {
            console.log(data);
            if (data.type == 'folder') {
                res.redirect(301, '/f/' + req.path.substr(2));
            }
            res.download(path);
        }, reason => {
            console.log(reason);
            res.status(reason[0]).json(reason);
        }
    );
});

app.post('/u/*', (req, res) => {
    console.log("upload file with path:" + req.path);
    console.log("and " + JSON.stringify(req.query));
    var path = './contents' + req.path.substr(2);
    var password = req.query.password ? req.query.password : null;
    var user = req.query.user ? req.query.user : null;
    var file_access = req.query.file_access ? req.query.file_access : {};

    console.log(req.query.folder=='false');

    if (req.query.folder && req.query.folder == 'true') {
        console.log('it\'s a create with name ' + req.query.name + '!');
        file_system.create_folder(path, req.query.name, { 'password': password, 'user': user }, file_access).then(
            data => {
                res.status(200).send('Success!');
            }, reason => {
                console.log(reason);
                res.status(500).send(reason);
            }
        );
    } else {
        var bsb = new busboy({ headers: req.headers });
        bsb.on('file', function (fieldname, file, filename, encoding, mimetype) {
            console.log('brb');
            console.log(fieldname, filename);
            file_system.upload_file(path, file, {
                'file_name': filename,
                'encoding': encoding,
                'mimetype': mimetype
            },
                { 'password': password, 'user': user },
                file_access);
        });
        bsb.on('finish', function () {
            res.writeHead(200, { 'Connection': 'close' });
            res.end('Success');
        });
        bsb.on('error', () => {
            res.writeHead(500, { 'Connection': 'close' });
            res.end('Fail');
        });
        return req.pipe(bsb);
    }
});

app.get('/r/*', (req, res) => {
    console.log('delete file:' + req.path);
    var path = './contents' + req.path.substr(2);
    var password = req.query.password ? req.query.password : null;
    var user = req.query.user ? req.query.user : null;
    file_system.delete_file(path, { 'password': password, 'user': user }).then(
        data => {
            res.status(200).send();
        }, reason => {
            console.log(reason);
            res.status(500).send(reason);
        }
    );
});

app.get('/', (req, res) => {
    console.log(req.path);
    html = fs.readFile('./index.html', {
        encoding: "utf-8",
        flag: "r"
    }).then(
        data => {
            res.status(200);
            res.type('html');
            res.send(data);
        }, reason => {
            console.log(reason);
            res.status(500).send("Internal server error!");
        }
    );
});

app.get('*', (req, res) => {
    console.log('404 query\n' + req.path);
    res.status(404).send();
});

app.listen(config.port, config.url, () => {
    console.log('listen on:');
    console.log(config);
})
